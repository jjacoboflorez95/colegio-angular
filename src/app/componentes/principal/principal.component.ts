import { Component, OnInit } from '@angular/core';
import { BodyAsignaturasCursos } from 'src/app/modelos/body-asignaturas.model';
import { BodyEstudiantes } from 'src/app/modelos/body-estudiantes.model';
import { ApirestAsignaturasService } from 'src/app/servicios/apirest-asignaturas.service';
import { ApirestProfesoresService } from 'src/app/servicios/apirest-profesores.service';
import { Profesores } from 'src/app/modelos/profesores.model';
import { ApirestEstudiantesService } from 'src/app/servicios/apirest-estudiantes.service';

/**
 * Creación: 2021/14/04
 * Autor: jacoboflorez
 * Clase que servirá, de la parte de typescript, para manejar la ventana principal.
 */
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  /**
   * Variable global que guardará los profesores consultados.
   */
  profesores: Profesores[];
  /**
   * Variable global que guardará los estudiantes consultados para un asignatura en específico.
   */
  estudiantes: BodyEstudiantes[];
  /**
   * Variable global que guardará las asignaturas y su curso, de un profesor en específico.
   */
  asignaturasCursos: BodyAsignaturasCursos[];
  /**
   * Variable global que guardará al profesor actual que haya eligido el usuario.
   */
  profesorElegido: string;
  /**
   * Variable global que guardará la asignatura actual que haya eligido el usuario.
   */
  asignaturaElegida: string;
  /**
   * Variable global que guardará el curso actual que haya eligido el usuario.
   */
  cursoElegido: string;
  /**
   * Variabel global que controlará si se muestra o no la sección de las asignaturas y cursos.
   */
  banderaAsignaturas: boolean;
  /**
   * Variabel global que controlará si se muestra o no la sección de los estudiantes.
   */
  banderaEstudiantes: boolean;

  constructor(protected apirestProfesoresService: ApirestProfesoresService, protected apirestAsignaturasService: ApirestAsignaturasService,
    protected ApirestEstudiantesService: ApirestEstudiantesService) {
    this.banderaAsignaturas = false;
    this.banderaEstudiantes = false;
  }

  ngOnInit(): void {
    this.buscarProfesores();
  }

  /**
   * Método que se encarga de ejecutar el servicio apirest encargado de consultar los profesores.
   * Este método se ejecuta al iniciar la aplicación.
   */
  buscarProfesores() {
    this.apirestProfesoresService.getProfesores()
      .subscribe(
        (data) => { // Success
          if (data.length != 0) {
            this.profesores = data;
          } else {
            console.log("Lista de profesores vacía. Por favor revisar");
          }
        },
        (error) => {
          console.log("Error susbscribe getProfesores");
          console.error(error);
        }
      );
  }

  /**
   * Método que se encargará de ejecutar el apirest encargado de consultar las empresas y su curso para
   * un profesor en específico.
   * Este método se ejecuta cada que se detecte un cambio en el select de los profesores.
   * @param opcionSeleccionada Recibirá el elemento option del html
   */
  buscarAsignaturas(opcionSeleccionada: HTMLSelectElement) {
    if (Number(opcionSeleccionada.value) > 0) {
      let idProfesor = this.profesores[Number(opcionSeleccionada.value) - 1].proId;
      this.apirestAsignaturasService.getAsignaturas(idProfesor)
        .subscribe(
          (data) => { // Success
            if (data.content.length != 0) {
              this.asignaturasCursos = data.content;
              this.profesorElegido = this.profesores[idProfesor - 1].proNombre;
              this.limpiarEstudiantes();
              this.banderaAsignaturas = true;   
            } else {
              console.log("Lista de asignaturas vacía. Por favor revisar");
            }
          },
          (error) => {
            console.log("Error susbscribe getAsignaturas");
            console.error(error);
          }
        );
    } else {
      this.limpiarVentana();
    }
  }

  /**
   * Método que se encargará de ejecutar el apirest encargado de consultar los estudiantes para
   * una asignatura en específico.
   * Este método se ejecuta cada que se detecte un clic en uan fila de la tabla de asignaturas y cursos.
   * @param filaSeleccionada Recibirá el elemento tr del html
   */
  buscarEstudiantes(filaSeleccionada: HTMLTableRowElement) {
    let idAsignatura = this.asignaturasCursos[filaSeleccionada.id].asiId;
    this.ApirestEstudiantesService.getEstudiantes(idAsignatura)
      .subscribe(
        (data) => { // Success
          if (data.content.length != 0) {
            this.estudiantes = data.content;
            this.asignaturaElegida = this.asignaturasCursos[filaSeleccionada.id].asiNombre;
            this.cursoElegido = this.asignaturasCursos[filaSeleccionada.id].curso;
            this.banderaEstudiantes = true;
          } else {
            console.log("Lista de estudiantes vacía. Por favor revisar");
          }
        },
        (error) => {
          console.log("Error susbscribe getEstudiantes");
          console.error(error);
        }
      );
  }

  /**
   * Método que se encargará de ocultar la sección de asignaturasCursos y la de estudiantes.
   */
  limpiarVentana() {
    this.asignaturasCursos = [];
    this.estudiantes = [];
    this.banderaAsignaturas = false;
    this.banderaEstudiantes = false;
  }

  /**
   * Método que se encarga de ocultar la sección de estudiantes.
   */
  limpiarEstudiantes() {
    this.estudiantes = [];
    this.banderaEstudiantes = false; 
  }
}
