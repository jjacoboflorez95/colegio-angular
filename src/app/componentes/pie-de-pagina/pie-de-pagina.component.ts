import { Component, OnInit } from '@angular/core';

/**
 * Creación: 2021/16/04
 * Autor: jacoboflorez
 * Clase que servirá, de la parte de typescript, para manejar el pie de página de las ventanas.
 */
@Component({
  selector: 'app-pie-de-pagina',
  templateUrl: './pie-de-pagina.component.html',
  styleUrls: ['./pie-de-pagina.component.css']
})
export class PieDePaginaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
