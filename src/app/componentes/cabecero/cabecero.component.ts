import { Component, OnInit } from '@angular/core';

/**
 * Creación: 2021/14/04
 * Autor: jacoboflorez
 * Clase que servirá, de la parte de typescript, para manejar el cabecero de las ventanas.
 */
@Component({
  selector: 'app-cabecero',
  templateUrl: './cabecero.component.html',
  styleUrls: ['./cabecero.component.css']
})
export class CabeceroComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
