/**
 * Creación: 2021/14/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar los profesores consultados.
 */
 export interface Profesores {

    proId: number;
    proNombre: string;
}