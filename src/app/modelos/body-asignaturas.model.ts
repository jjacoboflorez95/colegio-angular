/**
 * Creación: 2021/14/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar las asginaturas y cursos consultados.
 */
 export class BodyAsignaturasCursos {

    /**
     * 
     * @param asiId Variable que guardará el id de la asignatura.
     * @param asiNombre Variable que guardará el nombre de la asignatura.
     * @param asiCurso Variable que guardará el curso, que estar compuesto del grado + salón.
     */
    constructor(
        public asiId: number,
        public asiNombre: string,
        public curso: string) { }
}