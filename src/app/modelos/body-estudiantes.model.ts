/**
 * Creación: 2021/14/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar los estudiantes consultados.
 */
 export class BodyEstudiantes {

    /**
     * 
     * @param estId Variable que guardará el id del estudiante.
     * @param estNombre Variable que guardará el nombre del estudiante.
     */
    constructor(
        public estId: number,
        public estNombre: string) { }
}