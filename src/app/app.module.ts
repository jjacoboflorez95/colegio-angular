import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceroComponent } from './componentes/cabecero/cabecero.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { ApirestProfesoresService } from './servicios/apirest-profesores.service';
import { ApirestAsignaturasService } from './servicios/apirest-asignaturas.service';
import { ApirestEstudiantesService } from './servicios/apirest-estudiantes.service';
import { PieDePaginaComponent } from './componentes/pie-de-pagina/pie-de-pagina.component';

@NgModule({
  declarations: [
    AppComponent,
    CabeceroComponent,
    PrincipalComponent,
    PieDePaginaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [ApirestProfesoresService, ApirestAsignaturasService, ApirestEstudiantesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
