import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Creación: 2021/15/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar todos los apirest relacionados con las asignaturas.
 */
@Injectable({
  providedIn: 'root'
})
export class ApirestAsignaturasService {

  constructor(private http: HttpClient) { }

  /**
   * Método que se encarga de llamar al apirest que nos trae los datos de las asignaturas, junto con su curso,
   * de un profesor en específico.
   * @param idProfesor Recibe el id del profesor al que se le deben consultar las asignaturas.
   * @returns 
   */
  getAsignaturas(idProfesor: number): Observable<any> {
    let url: string = "http://localhost:8080/colegio-apirest/asignatura/idprofesor/" + idProfesor;
    return this.http.get(url);
  }
}
