import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Creación: 2021/15/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar todos los apirest relacionados con los profesores.
 */
@Injectable({
  providedIn: 'root'
})
export class ApirestProfesoresService {

  constructor(private http: HttpClient) { }

  /**
   * Método que se encarga de llamar al apirest que nos trae los datos de los profesores
   * @returns 
   */
  getProfesores(): Observable<any> {
    let url: string = "http://localhost:8080/colegio-apirest/profesor";
    return this.http.get(url);
  }
}
