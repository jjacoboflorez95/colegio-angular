import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Creación: 2021/15/04
 * Autor: jacoboflorez
 * Clase que servirá para guardar todos los apirest relacionados con los estudiantes.
 */
@Injectable({
  providedIn: 'root'
})
export class ApirestEstudiantesService {

  constructor(private http: HttpClient) { }

  /**
   * Método que se encarga de llamar al apirest que nos trae los datos de los estudiantes
   * de una asignatura en específico.
   * @param idAsignatura Recibe el id de la asignatura a la que se le deben consultar los estudiantes.
   * @returns 
   */
  getEstudiantes(idAsignatura: number): Observable<any> {
    let url: string = "http://localhost:8080/colegio-apirest/estudiante/idasignatura/" + idAsignatura;
    return this.http.get(url);
  }
}
